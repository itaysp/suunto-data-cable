# Suunto Data Cable with ESP32

Read more here:
[Making a DIY data cable for the Zoop Novo Diving Computer](http://itay.mobi/blog/?p=286)

I'm Using Lolin32 Lite here, but any ESP32 will do the job.  
The Lolin32 use CH430 for Serial Communication with the PC.  
Attached a modified driver for the CH430 that change its name. 

If you are using ESP32 that has other chip, you will need to modify the driver.  

![Schematic](Photos/connections.png)

![Connector](../Photos/Connector.png)
